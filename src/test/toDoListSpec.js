describe('Controller: todoCtrl', function () {
    'use strict';

    var todoCtrl,
        scope;

    beforeEach(module('demoApp'));

    beforeEach(inject(function ($controller, $rootScope) {
        scope = $rootScope.$new();
        todoCtrl = $controller('todoCtrl', {
            $scope : scope
        });
    }));

    it('Todos should be defined', function () {
        expect(scope.todos).toBeDefined();
    });

    it("Shouldn't have todos", function(){
        expect(scope.todos.length).toEqual(0);
    });

    it("Should delete all", function(){
        var todo = {}
        scope.todos = [{}, todo, {}]
        expect(scope.todos.length).toEqual(3);
        scope.removeAllTodo()
        expect(scope.todos.length).toEqual(0)
    })
});
